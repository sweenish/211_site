import 'package:flutter/material.dart';

class ExpectationsPage extends StatelessWidget {
  ExpectationsPage({Key key, this.title}) : super(key: key);

  final String title;
  final String bullet = "\u2022 ";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Center(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Spacer(),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Expectations",
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
            ),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        style: Theme.of(context).textTheme.bodyText1,
                        children: <TextSpan>[
                          TextSpan(text: "$bullet Be respectful.\n\n"),
                          TextSpan(text: "$bullet Attend lecture and lab.\n\n"),
                          TextSpan(
                              text:
                                  "$bullet 8 - 12 hours of self-study per week "
                                  "per the definition of a credit hour.\n\n"),
                          TextSpan(text: "$bullet Particpate in lecture.\n\n"),
                          TextSpan(text: "$bullet Utilize the lab time.\n\n"),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
