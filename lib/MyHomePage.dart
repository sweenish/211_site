import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intro_site/colors.dart';
import 'package:url_launcher/url_launcher.dart';

import 'ExpectationsPage.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(title),
      ),
      body: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              ShaderMask(
                child: Image.asset("assets/images/james-harrison.jpg"),
                shaderCallback: (Rect bounds) {
                  return LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [Colors.transparent, Colors.white],
                      stops: [0.0, 0.7]).createShader(bounds);
                },
              ),
              Text(
                "WSU CS 211",
                style: Theme.of(context)
                    .textTheme
                    .headline3, // Create a white text theme
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10.0,
              left: 20.0,
              right: 20.0,
            ),
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                style: Theme.of(context).textTheme.bodyText1,
                children: <TextSpan>[
                  TextSpan(text: "Welcome to "),
                  TextSpan(
                      text: "CS 211: Introduction to Programming",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                  TextSpan(
                      text:
                          "! Use the menu to read about high level expectations, "
                          "creating a coding environment on your on computer, "
                          "and to view the git repository where lectures are "
                          "kept and updated."),
                ],
              ),
            ),
          ),
        ],
      ),
      drawer: Drawer(
          child: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 9),
            child: ListTile(
              leading: Image.asset("assets/images/wsu_logo.png"),
            ),
          ),
          Divider(
            indent: 15,
            endIndent: 15,
            thickness: 0.5,
            color: WuColors.huxley,
          ),
          ListTile(
            title: Text('Expectations'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return ExpectationsPage(
                    title: "Expectations",
                  );
                },
              ));
            },
          ),
          ListTile(
            title: Text('Setting up your computer'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Course Content'),
            onTap: () {
              Navigator.pop(context);
              launch("https://gitlab.com/sweenish/cs211",
                  enableJavaScript: true);
            },
          ),
          AboutListTile(
            applicationName: "CS 211",
            applicationVersion: "2021-02-02",
            aboutBoxChildren: <Widget>[
              Linkify(
                  onOpen: (link) {
                    launch(link.url, enableJavaScript: true);
                  },
                  text: "This site was created using the Flutter framework. "
                      "It is hosted for free by GitLab. To view the code, "
                      "visit the repository at "
                      "https://gitlab.com/sweenish/211_site.\n\n"
                      "Credits:\nHero image: James Harrison\n"
                      "https://unsplash.com/@jstrippa\n\n"
                      "Colors:\nWSU Branding:\n"
                      "https://www.wichita.edu/services/strategic_communications/brand_standards/colors_and_fonts.php\n\n"
                      "Material Design Palette Generator:\n"
                      "http://mcg.mbitson.com/#!?mcgpalette0=%23c8c3c1"),
            ],
          ),
        ],
      )),
    );
  }
}
