import 'package:flutter/material.dart';

class WuColors {
  //
  // Shocker Yellow
  //
  static const MaterialColor shockeryellow = MaterialColor(_shockeryellowPrimaryValue, <int, Color>{
    50: Color(0xFFFFF8E3),
    100: Color(0xFFFFEDB9),
    200: Color(0xFFFFE18B),
    300: Color(0xFFFFD45D),
    400: Color(0xFFFFCB3A),
    500: Color(_shockeryellowPrimaryValue),
    600: Color(0xFFFFBC14),
    700: Color(0xFFFFB411),
    800: Color(0xFFFFAC0D),
    900: Color(0xFFFF9F07),
  });
  static const int _shockeryellowPrimaryValue = 0xFFFFC217;

  static const MaterialColor shockeryellowAccent = MaterialColor(_shockeryellowAccentValue, <int, Color>{
    100: Color(0xFFFFFFFF),
    200: Color(_shockeryellowAccentValue),
    400: Color(0xFFFFE5C1),
    700: Color(0xFFFFDAA7),
  });
  static const int _shockeryellowAccentValue = 0xFFFFFAF4;

  //
  // Huxley
  //
  static const MaterialColor huxley = MaterialColor(_huxleyPrimaryValue, <int, Color>{
  50: Color(0xFFF8F8F8),
  100: Color(0xFFEFEDEC),
  200: Color(0xFFE4E1E0),
  300: Color(0xFFD9D5D4),
  400: Color(0xFFD0CCCA),
  500: Color(_huxleyPrimaryValue),
  600: Color(0xFFC2BDBB),
  700: Color(0xFFBBB5B3),
  800: Color(0xFFB4AEAB),
  900: Color(0xFFA7A19E),
});
static const int _huxleyPrimaryValue = 0xFFC8C3C1;

static const MaterialColor huxleyAccent = MaterialColor(_huxleyAccentValue, <int, Color>{
  100: Color(0xFFFFFFFF),
  200: Color(_huxleyAccentValue),
  400: Color(0xFFFFEEE7),
  700: Color(0xFFFFDCCE),
});
static const int _huxleyAccentValue = 0xFFFFFFFF;

//
// Wheat
//
static const MaterialColor wheat = MaterialColor(_wheatPrimaryValue, <int, Color>{
  50: Color(0xFFFEF5E4),
  100: Color(0xFFFBE6BB),
  200: Color(0xFFF9D68E),
  300: Color(0xFFF7C660),
  400: Color(0xFFF5B93E),
  500: Color(_wheatPrimaryValue),
  600: Color(0xFFF1A619),
  700: Color(0xFFEF9C14),
  800: Color(0xFFED9311),
  900: Color(0xFFEA8309),
});
static const int _wheatPrimaryValue = 0xFFF3AD1C;

static const MaterialColor wheatAccent = MaterialColor(_wheatAccentValue, <int, Color>{
  100: Color(0xFFFFFFFF),
  200: Color(_wheatAccentValue),
  400: Color(0xFFFFD6AD),
  700: Color(0xFFFFC994),
});
static const int _wheatAccentValue = 0xFFFFF0E0;
}