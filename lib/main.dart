import 'package:flutter/material.dart';
import 'colors.dart';
import 'MyHomePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wichita State University CS 211',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: WuColors.shockeryellow,
        accentColor: WuColors.wheat,
        fontFamily: 'Roboto',
        textTheme: TextTheme(
            headline3: TextStyle(color: Colors.black),
            bodyText1: TextStyle(color: Colors.black, fontSize: 18)),
      ),
      home: MyHomePage(title: 'Wichita State University CS 211'),
    );
  }
}
