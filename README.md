# intro_site

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Attributions:
Image: <span>Photo by <a href="https://unsplash.com/@jstrippa?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">James Harrison</a> on <a href="https://unsplash.com/@jstrippa?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
Color swatches: [WSU Branding](https://www.wichita.edu/services/strategic_communications/brand_standards/colors_and_fonts.php) & [Material Design Palette Generator](http://mcg.mbitson.com/#!?mcgpalette0=%23c8c3c1)